package aMAZEingSolver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Maze {
	private RenderMaze renderMaze;
	private boolean found = false;
	private int[][] mazeArray;
	private int[] cursor = new int[] { 0, 0 };
	private ArrayList<int[]> walked = new ArrayList<>();
	private ArrayList<int[]> shortest = new ArrayList<>();
	private ArrayList<MazeNode> nodes = new ArrayList<>();
	private int rows;
	private int cols;

	public Maze() {
		this.mazeArray = this.getMazeArray();
		renderMaze = new RenderMaze(this);
	}

	/**
	 * all getters and setters
	 */
	
	public ArrayList<MazeNode> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<MazeNode> nodes) {
		this.nodes = nodes;
	}
	
	public boolean isFound() {
		return found;
	}

	public void setFound(boolean found) {
		this.found = found;
	}

	public ArrayList<int[]> getShortest() {
		return shortest;
	}

	public void setShortest(ArrayList<int[]> shortest) {
		this.shortest = shortest;
	}

	public int[] getCursor() {
		return cursor;
	}

	public void setCursor(int[] cursor) {
		this.cursor = cursor;
	}

	public int[] getDim() {
		int[] dim = new int[] { cols, rows };
		return dim;
	}

	public int getRows() {
		return this.rows;
	}

	public int getCols() {
		return this.cols;
	}

	public int[][] getMazeArray() {

		String FILENAME = "C:\\Users\\patty\\OneDrive\\algoritmen\\MazeSolver\\maze.txt";
		String mazeString = "";
		BufferedReader br = null;
		FileReader fr = null;
		int[][] mazeArray;

		try {
			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				cols = sCurrentLine.length();
				rows++;
				mazeString += (sCurrentLine + System.lineSeparator());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		mazeArray = new int[rows][cols];
		Scanner scanner = new Scanner(mazeString);
		int i = 0;
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			for (int j = 0; j < cols; j++) {
				char c = line.charAt(j);
				if ((int) c == 88) {
					mazeArray[i][j] = 1;
				} else {
					mazeArray[i][j] = 0;
				}
			}
			i++;
		}
		scanner.close();
		return mazeArray;
	}

	public ArrayList<int[]> getWalked() {
		return this.walked;
	}

	/**
	 * 
	 * @return start position put into a MazeNode
	 */
	public MazeNode findStart() {
		MazeNode start = new MazeNode();
		for (int i = 0; i < rows; i++) {
			if (mazeArray[i][0] == 0) {
				start.setCoord(new int[] { 0, i });
				break;
			}
		}
		return start;
	}

	/**
	 * 
	 * @return end position put into a mazenode
	 */
	public MazeNode findEnd() {
		MazeNode end = new MazeNode();
		for (int i = 0; i < rows; i++) {
			if (mazeArray[i][cols - 1] == 0) {
				end.setCoord(new int[] { cols - 1, i });
				break;
			}
		}
		return end;
	}

	/**
	 * Check if a given coord is a free space in the maze
	 * 
	 * @param coord
	 *            of the position that needs to be checked
	 * @return true or false
	 */
	public boolean checkFree(int[] coord) {
		if (mazeArray[coord[1]][coord[0]] == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if the arraylist of the walked coordinates contains a given coordinate
	 * 
	 * @param coords
	 * @return true or false
	 */
	public boolean walkedContains(int[] coords) {
		for (int i = 0; i < walked.size(); i++) {
			int[] cursor = walked.get(i);
			if ((cursor[0] == coords[0]) && (cursor[1] == coords[1])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if the arraylist of the nodes contains a given coordinate
	 * 
	 * @param coords
	 * @return true or false
	 */
	public boolean nodesContains(int[] coords) {
		for (int i = 0; i < nodes.size(); i++) {
			MazeNode cursor = nodes.get(i);
			if ((cursor.getCoord()[0] == coords[0]) && (cursor.getCoord()[1] == coords[1])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Delete all the coords that have been added after a certain coordinate
	 * 
	 * @param coords
	 */
	public void deleteLast(int[] coords) {
		while (!(shortest.get(0)[0] == coords[0] && shortest.get(0)[1] == coords[1])) {
			shortest.remove(0);
		}
	}

	/**
	 * this method was used for debugging only, it prints out the list of nodes
	 */
	public void printNodes() {
		for (MazeNode node : nodes) {
			System.out.println("Coords: [" + node.getCoord()[0] + "," + node.getCoord()[1] + "]");
			System.out.println("Direction to go: " + node.getDirec());
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		}
	}
	/**
	 * this method was used for debugging only, it prints out all the coords in the
	 * list of the shortest path
	 */
	public void printShortest() {
		for (int[] node : shortest) {
			System.out.println("Coords: [" + node[0] + "," + node[1] + "]");
		}
	}
	/**
	 * Method that searches trough the maze
	 */
	public void walkMaze() {
		cursor = this.findStart().getCoord();
		int[] endCoord = this.findEnd().getCoord();
		// adding the start position to nodes
		nodes.add(0, new MazeNode(new int[] { cursor[0], cursor[1] }));
		nodes.get(0).setDirec(Direction.Right);
		shortest.add(0, cursor);
		walked.add(0, cursor);
		cursor[0] += 1;
		while (!found) {
			int vrijeCellen = 0;
			int cx = cursor[0];
			int cy = cursor[1];
			if (cx == this.findEnd().getCoord()[0] && cy == this.findEnd().getCoord()[1]) {
				found = true;
				break;
			}
			shortest.add(0, cursor);
			walked.add(0, cursor);
			int[] left = new int[] { cx - 1, cy };
			int[] right = new int[] { cx + 1, cy };
			int[] up = new int[] { cx, cy - 1 };
			int[] down = new int[] { cx, cy + 1 };

			try {
				//count the number of free places around the cursor
				if (checkFree(left) && !walkedContains(left)) {
					vrijeCellen++;
				} if (checkFree(right) && !walkedContains(right)) {
					vrijeCellen++;
				} if (checkFree(up) && !walkedContains(up)) {
					vrijeCellen++;
				} if (checkFree(down) && !walkedContains(down)) {
					vrijeCellen++;
				} 
				/**
				 * walk a certain direction depending on the number of free places
				 */
				if (vrijeCellen >= 1) {
					if (!nodesContains(cursor) && vrijeCellen > 1) {
						nodes.add(0, new MazeNode(cursor));
					}
					if (checkFree(right) && !walkedContains(right)) {
						if (nodesContains(cursor)) {
							nodes.get(0).setDirec(Direction.Right);
						}
						cursor = right;
					} else if (checkFree(down) && !walkedContains(down)) {
						if (nodesContains(cursor)) {
							nodes.get(0).setDirec(Direction.Down);
						}
						cursor = down;
					} else if (checkFree(up) && !walkedContains(up)) {
						if (nodesContains(cursor)) {
							nodes.get(0).setDirec(Direction.Up);
						}
						cursor = up;
					} else if (checkFree(left) && !walkedContains(left)) {
						if (nodesContains(cursor)) {
							nodes.get(0).setDirec(Direction.Left);
						}
						cursor = left;
					}
				}
				/**
				 * if the cursor has hit a dead end we fall back to the last node and check if that one has free places, if not we continue to go back in the nodes list
				 */
				if (vrijeCellen == 0) {
					if (deadEnd(cursor)) {
						cursor = nodes.get(0).getCoord();
						deleteLast(nodes.get(0).getCoord());
					} else {
						deleteLast(nodes.get(0).getCoord());
						nodes.remove(0);
						cursor = nodes.get(0).getCoord();
						deleteLast(nodes.get(0).getCoord());
					}
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			// the code below allows to see the pathfinding algorithm working step by step
			/*
			 * renderMaze.repaint(); try { Thread.sleep(10); } catch (InterruptedException
			 * ex) { Thread.currentThread().interrupt(); }
			 */
		}
	}
	/**
	 * Checks if a given position is a dead end or not
	 * @param cursor
	 * @return
	 */
	public boolean deadEnd(int[] cursor) {
		int walls = 0;
		int cx = cursor[0];
		int cy = cursor[1];
		int[] left = new int[] { cx - 1, cy };
		int[] right = new int[] { cx + 1, cy };
		int[] up = new int[] { cx, cy - 1 };
		int[] down = new int[] { cx, cy + 1 };
		if (!checkFree(left)) {
			walls++;
		}
		if (!checkFree(right)) {
			walls++;
		}
		if (!checkFree(up)) {
			walls++;
		}
		if (!checkFree(down)) {
			walls++;
		}
		if (walls == 3) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Maze maze = new Maze();
		maze.renderMaze.repaint();
		System.out.println("start solving maze");
		long startTime = System.currentTimeMillis();
		maze.walkMaze();
		long endTime = System.currentTimeMillis();
		System.out.println("That took " + (endTime - startTime) + " milliseconds");
		maze.renderMaze.repaint();
	}
}
