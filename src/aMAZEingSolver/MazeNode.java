package aMAZEingSolver;
/**
 * 
 * @author Pattyn Baptiste
 * Class that defines a node from the maze
 * contains a direction and a coord
 *
 */
public class MazeNode {
	private Direction direc;
	private int[] coord = new int[2];
	
	public MazeNode() {}
	
	public MazeNode(int[] coord) {
		this.coord = coord;
	}

	public Direction getDirec() {
		return direc;
	}

	public void setDirec(Direction direc) {
		this.direc = direc;
	}

	public int[] getCoord() {
		return coord;
	}

	public void setCoord(int[] coord) {
		this.coord = coord;
	}
	
	
}
