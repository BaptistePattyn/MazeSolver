package aMAZEingSolver;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class RenderMaze extends JPanel {
	private JFrame frame;
	private Maze maze;
	private int blockHeight = 1;
	private int blockWidth = 1;

	public RenderMaze(Maze maze) {
		this.maze = maze;
		this.setBackground(Color.white);
		frame = new JFrame();
		frame.setSize((maze.getCols() + 1) * blockWidth, (maze.getRows() + 2) * blockHeight);
		frame.setContentPane(this);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Mazesolver");
		frame.setVisible(true);
		setFocusable(true);
		frame.validate();
		frame.repaint();
	}
/**
 * paint all the objects on the JFrame
 */
	public void paintComponent(Graphics g) {
		if (maze.isFound()) {
			super.paintComponent(g);
			int xcoord = 0;
			int ycoord = 0;
			//draw the maze
			int[][] mazeArray = this.maze.getMazeArray();
			for (int i = 0; i < this.maze.getRows(); i++) {
				for (int j = 0; j < this.maze.getCols(); j++) {
					if (mazeArray[i][j] == 1) {
						g.setColor(Color.BLACK);
						g.fillRect(xcoord * blockWidth, ycoord * blockHeight, this.blockWidth, this.blockHeight);
						g.setColor(Color.WHITE);
						g.drawRect(xcoord * blockWidth, ycoord * blockHeight, this.blockWidth, this.blockHeight);
					}
					xcoord++;
				}
				xcoord = 0;
				ycoord++;
			}

			// draw the starting point
			g.setColor(Color.yellow);
			g.fillRect(maze.findStart().getCoord()[0] * blockWidth, maze.findStart().getCoord()[1] * blockHeight,
					this.blockWidth, this.blockHeight);
			// draw the end point
			g.setColor(Color.orange);
			g.fillRect(maze.findEnd().getCoord()[0] * blockWidth, maze.findEnd().getCoord()[1] * blockHeight,
					this.blockWidth, this.blockHeight);
			// draw walked
			if (!maze.getWalked().isEmpty()) {
				g.setColor(Color.red);
				for (int[] cell : maze.getWalked()) {
					g.fillRect(cell[0] * blockWidth, cell[1] * blockHeight, this.blockWidth, this.blockHeight);
				}
			}
			// draw the nodes that have been encountered
			if(!maze.getNodes().isEmpty()) {
				g.setColor(Color.GRAY);
				for(MazeNode node : maze.getNodes()) {
					g.fillRect(node.getCoord()[0]*blockWidth, node.getCoord()[1]*blockHeight, blockWidth, blockHeight);
				}
			}
			
			// draw cursor
			g.setColor(Color.MAGENTA);
			g.fillRect(maze.getCursor()[0] * blockWidth, maze.getCursor()[1] * blockHeight, this.blockWidth,this.blockHeight);
			
			// draw the shortest path
			if(!maze.getShortest().isEmpty() && maze.isFound()) {
				g.setColor(Color.GREEN);
				for(int[] node : maze.getShortest()) {
					g.fillRect(node[0]*blockWidth, node[1]*blockHeight, blockWidth, blockHeight);
				}
			}

		}
	}
}
